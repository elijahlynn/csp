output "csp_flight_review_url_add" {
  description = "URL to add a flight review"
  value       = "${aws_apigatewayv2_stage.lambda.invoke_url}/add"
}

output "csp_flight_review_url_view_all" {
  description = "URL to view all flight reviews"
  value       = "${aws_apigatewayv2_stage.lambda.invoke_url}/view/all"
}
