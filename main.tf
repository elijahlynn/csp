terraform {
  # Causing error in GitLab CI "Error: Unsupported Terraform Core version"
  # required_version = ">=1.4.6"

  backend "http" {
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
    random = {
      source  = "hashicorp/random"
      version = ">= 2.0"
    }
  }
}

provider "aws" {
  region = "us-west-2"
}

resource "aws_lambda_function" "csp_flight_review" {
  filename      = "${local.building_path}/${local.lambda_code_filename}"
  handler       = "index.lambda_handler"
  runtime       = "python3.8"
  function_name = "cspFlightReview"
  role          = aws_iam_role.iam_for_csp_flight_review.arn
  timeout       = 30
  depends_on = [
    null_resource.build_lambda_function
  ]
}

// The `sam` CLI tool looks for `sam_metadata` prefix in the resource name to identify the resource as a SAM resource.
resource "null_resource" "sam_metadata_aws_lambda_function_csp_flight_review" {
  triggers = {
    resource_name        = "aws_lambda_function.csp_flight_review"
    resource_type        = "ZIP_LAMBDA_FUNCTION"
    original_source_code = "${local.lambda_src_path}"
    built_output_path    = "${local.building_path}/${local.lambda_code_filename}"
  }
  depends_on = [
    null_resource.build_lambda_function
  ]
}

resource "null_resource" "build_lambda_function" {
  triggers = {
    lambda_function_hash = local.lambda_function_hash
  }

  provisioner "local-exec" {
    command = "./py_build.sh \"${local.lambda_src_path}\" \"${local.building_path}\" \"${local.lambda_code_filename}\" Function"
  }
}

resource "aws_iam_role" "iam_for_csp_flight_review" {
  name = "iam_for_csp_flight_review"

  managed_policy_arns = ["arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole"]

  assume_role_policy = <<EOF
    {
    "Version": "2012-10-17",
    "Statement": [
        {
        "Action": "sts:AssumeRole",
        "Principal": {
            "Service": "lambda.amazonaws.com"
        },
        "Effect": "Allow",
        "Sid": ""
        }
    ]
    }
    EOF

}


## API Gateway

resource "aws_apigatewayv2_api" "lambda" {
  name          = "csp_flight_review_service"
  protocol_type = "HTTP"
  cors_configuration {
    allow_headers = ["*"]
    allow_methods = ["GET", "POST"]
    allow_origins = ["*"]
    max_age       = 300
  }
}

resource "aws_apigatewayv2_stage" "lambda" {
  api_id = aws_apigatewayv2_api.lambda.id

  name        = "flight-reviewer"
  auto_deploy = true

  access_log_settings {
    destination_arn = aws_cloudwatch_log_group.api_gw.arn

    format = jsonencode({
      requestId               = "$context.requestId"
      sourceIp                = "$context.identity.sourceIp"
      requestTime             = "$context.requestTime"
      protocol                = "$context.protocol"
      httpMethod              = "$context.httpMethod"
      resourcePath            = "$context.resourcePath"
      routeKey                = "$context.routeKey"
      status                  = "$context.status"
      responseLength          = "$context.responseLength"
      integrationErrorMessage = "$context.integrationErrorMessage"
      }
    )
  }
}

resource "aws_apigatewayv2_integration" "csp_flight_review_api" {
  api_id = aws_apigatewayv2_api.lambda.id

  integration_uri    = aws_lambda_function.csp_flight_review.invoke_arn
  integration_type   = "AWS_PROXY"
  integration_method = "POST"
}

resource "aws_apigatewayv2_route" "csp_flight_review_route_get" {
  api_id    = aws_apigatewayv2_api.lambda.id
  route_key = "GET /view/all"
  target    = "integrations/${aws_apigatewayv2_integration.csp_flight_review_api.id}"
}

resource "aws_apigatewayv2_route" "csp_flight_review_route_post" {
  api_id    = aws_apigatewayv2_api.lambda.id
  route_key = "POST /add"
  target    = "integrations/${aws_apigatewayv2_integration.csp_flight_review_api.id}"
}

resource "aws_cloudwatch_log_group" "api_gw" {
  name = "/aws/api_gw/${aws_apigatewayv2_api.lambda.name}"

  retention_in_days = 30
}

resource "aws_lambda_permission" "api_gw" {
  statement_id  = "AllowExecutionFromAPIGateway"
  action        = "lambda:InvokeFunction"
  function_name = aws_lambda_function.csp_flight_review.function_name
  principal     = "apigateway.amazonaws.com"

  source_arn = "${aws_apigatewayv2_api.lambda.execution_arn}/*/*"
}
