# Overview

The goal of this project was to demonstrate security and technical capability by creating an API and deploying it to a Cloud of my choosing.

I chose:

* AWS as that is the cloud I am familiar with
* GitLab to host the repo
* GitLab to create issues and track work
* GitLab CI service to deploy the API to AWS
* Terraform for Ifrastructure as Code (IaC) (w/GitLab TF State Service)
* Terraform deploy pattern that I found in the AWS Serverless Architecture Model (SAM) [documentation](https://aws.amazon.com/blogs/compute/better-together-aws-sam-cli-and-hashicorp-terraform/)
* API
     * Flight Reviewer - Add flight reviews, and see all reviews
     * Public API, no auth for now

# Demo

* HOST - https://7mdllxeuj1.execute-api.us-west-2.amazonaws.com
* GET  - /flight-reviewer/view/all - Will return list of all flight reviews in JSON response
* POST - /flight-reviewer/add      - Will accept JSON body with new flight review

TODO: Example curl commands

No Update or Delete operations for now

## Authorization & Authentication

* API: Public Accessible, no API Key needed

* Deployment: I created a `csp-admin` user and group 

# Architecture

## High-Level

![](docs/images/csp-architecture-20230604-1.png)

## Deploy

## API

# Security 

Security starts with credentials, so I used an AWS root user with MFA enabled (MFA provided with 1Password One-Time Passwords (OTP)) to create the IAM `csp-admin` user that is used for CLI and automation operations. I created a security policy  and group for this user. 

Security Policy discovery: A nice trick I learned was by granting the policy full access to to the services it needed, so  apigateway:*, labmda:*, etc. Then after everything was working, I used the AWS Access Advisor, which tells about permissions that are in the policy but haven't been used. This gave me a list of permissions that could be revoked. It would be nice to find or build a tool to automate this. 


KICS' (Keeping Infrastructure as Code Secure) Static Application Securiting Testing (SAST) for scanning IaC code https://kics.io/ which runs in the test stage for the CI Pipeline.


# Challenges

1. Initially tried out the Serverless Framework to deploy a Lambda but encountered [an issue](https://gitlab.com/gitlab-org/project-templates/serverless-framework/-/issues/12) with the NodeJS runtime, and found that the Serverless GitLab CI Template is deprecated so left an [upstream comment](htthereps://gitlab.com/groups/gitlab-org/configure/-/epics/6#note_1416879818) letting them know that the template doesn't work and urged them to delete it
1. Had to make an [upstream PR contribution](github.com/aws-samples/aws-sam-terraform-examples/issues/8) to the AWS SAM (Serverless Architecture Model) example repository to fix an issue with the urllib3 upstream dependency for their Python API example. 
1. Not sure how this architecture would work yet for PR/MR environments, but I think it would involve using a SAM container image with the `sam` CLI command to replicate the Lambda container environment, just like works locally. 
1. Created an [upstream issue](https://gitlab.com/elijahlynn/csp/-/issues/3) created to debug Terraform not detecting Lambda function changes


# Further Improvments

1. Automated security testing with GitLab CI
    1. Static Code Analaysis on the Application code (currently only on IaC) either with GitLab Ultimate or custom CI Pipelines
1. Request per/minute throttling (not really a security risk with Lambda, but a billing issue). Would be security risk if using finite resource compute. 
1. If application required, implement API keys to control Authentication & Authorization 
1. Better Monitoring and Logging - Grafana Dashboards
1. Shift Left: Add VS Code Checkmarx (KICS) extension scanning locally for dev environments, before even committing, reducing cycle times. 
