locals {
  building_path        = "build"
  lambda_code_filename = "csp-flight-review.zip"
  lambda_src_path      = "./src"
  lambda_function_hash = filebase64sha256("${local.lambda_src_path}/index.py")
}