# CSP

## Overview
This repo is to help demonstrate security knowledge and technical capabilities for Elijah Lynn. 

This repo is public and contains no secrets. Public repos can be a security enhancement as it naturally creates better security practices, allows for source code observability and reporting by outside ethical hackers and doesn't rely on security by obscurity. 

## Authentiction & Authorization Setup

Since we need credentials to do automation to begin with (chicken vs egg problem) we need to do some manual configuration for authentication and authorization in IAM so that we can use the `aws` & `sam` and `terraform` CLI tooling to provision infrastructure and automate deployments.

### AWS

1. Login with root AWS user
1. Manually create new IAM group `csp-admin`
1. Manually create new IAM policy `CSP-Admin` with the policy in the iam-policy-csp-admin.iam. 
1. Attach `CSP-Admin` policy to the `csp-admin` group
1. Manually configure IAM user `csp-admin`
    1. Do not allow user access to Console
1. Add `csp-admin` user to `csp-admin` group
1. Create access credentials for `csp-admin` user
1. Setup local AWS credentials with `aws configure`, use `us-west-2` region

### GitLab

1. Add the following AWS Access credentials to [GitLab CI/CD environment variables](https://gitlab.com/elijahlynn/csp/-/settings/ci_cd) for AWS IAM user `csp-admin`
    1. AWS_SECRET_ACCESS_KEY - Must be masked to avoid being output in logs
    1. AWS_ACCESS_KEY_ID - Do not mask this, this can be in log output, very useful to have this ID for debugging access errors, to know which ID it is for, if masked, then impossible to know in logs
    1. AWS_DEFAULT_REGION - us-west-2

## Terraform State Setup

We use GitLab's provided Terraform State service. 

1. Run pipeline once to initialize the Terraform State
1. View state here https://gitlab.com/elijahlynn/csp/-/terraform
1. Click the three dots and "Copy Terraform init command" and run that locally on the repo

## Deployments

You can deploy locally with Terraform or deploy on push with GitLab CI. 

1. Local: `terraform plan` then `terraform apply`
1. Remote: `git push` then GitLab CI will run Pipeline and deploy, see https://gitlab.com/elijahlynn/csp/-/pipelines

## Application Development

WIP

## Tear Down

1. `terraform destroy`
    1. This will remove:
        1. Dynamo DB
        1. Cloudwatch Logs Group
        1. API Gateway
        1. Lambda
1. Manually delete:
    1. `csp-admin` user
    1. `csp-admin` group
    1. `CSP-Admin` policy














