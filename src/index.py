import json

def sanitize_input(input_str):
    # Replace any characters that are not alphanumeric or whitespace with an underscore
    return ''.join(c if c.isalnum() or c.isspace() else '_' for c in input_str)

def lambda_handler(event, context):
    http_method = event["httpMethod"]
    if http_method == "GET":
        # Handle GET request
        response = {
            "statusCode": 200,
            "body": {
                "message": "This will eventually return all flight reviews"
            }
        }
    elif http_method == "POST":
        # Handle POST request
        request_body = json.loads(event["body"])
        sanitized_body = {}
        for key, value in request_body.items():
            # Sanitize each key and value in the request body
            sanitized_key = sanitize_input(key)
            sanitized_value = sanitize_input(value)
            sanitized_body[sanitized_key] = sanitized_value
        response = {
            "statusCode": 200,
            "body": {
                "message": f"This will eventually accept a new flight review in JSON format: {sanitized_body}"
            }
        }
    else:
        # Handle unsupported HTTP method
        # This path should never run as API Gateway will reject unsupported HTTP methods
        response = {
            "statusCode": 400,
            "body": {
                "message": f"Unsupported HTTP method: {http_method}"
            }
        }
    
    return {
        "statusCode": response["statusCode"],
        "body": json.dumps(response["body"])
    }